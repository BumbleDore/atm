import React from "react";
import ReactDOM from "react-dom";
import { shallow, mount } from "enzyme";
import PinPad from "./components/Pad";
import PadPrompt from "./components/PadPrompt";
import { act } from "react-dom/test-utils";
import App from "./App";
import { signinRequest } from "./api";
import Alert from "./components/Alert";

let wrapped;

beforeEach(() => {
  fetch.resetMocks();
  wrapped = shallow(<App />);
});

afterEach(() => {
  wrapped.unmount();
});

const padButtonPresses = (app, buttons) => {
  for (let press of buttons) {
    app
      .find("button")
      .find("span")
      .filterWhere((n) => n.text() === press)
      .simulate("click");
  }
};

describe("Api call and Response", () => {
  it("Will call api and return currentBalance of 300 ", async () => {
    fetch.mockResponseOnce(JSON.stringify({ currentBalance: 300 }));
    const res = await signinRequest(1234);
    expect(res.currentBalance).toEqual(300);
  });

  it("Will call api and fail returning false", async () => {
    fetch.mockResponseOnce(JSON.stringify({ currentBalance: 300 }), {
      status: 403,
    });
    const res = await signinRequest(1234);
    expect(res).toBeFalsy();
  });
});

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<App />, div);
});

it("renders Pin Pad prompt", () => {
  expect(wrapped.find(PadPrompt).length).toEqual(1);
});

it("shows a PinPad", () => {
  expect(wrapped.find(PinPad).length).toEqual(1);
});

describe("App", () => {
  it("dispalys balance after login", async () => {
    const app = mount(<App />);

    fetch.mockResponseOnce(JSON.stringify({ currentBalance: 300 }));

    await act(async () => {
      padButtonPresses(app, ["Enter"]);
    });
    app.update();
    expect(
      app
        .find("p")
        .filterWhere((n) => n.text() === "£300.00")
        .text()
    ).toEqual("£300.00");
  });

  it("dispalys alert after failed login", async () => {
    const app = mount(<App />);

    fetch.mockResponseOnce(JSON.stringify({ currentBalance: 300 }), {
      status: 403,
    });

    await act(async () => {
      padButtonPresses(app, ["Enter"]);
    });
    app.update();
    expect(document.documentElement.outerHTML).toContain("Bad Login");
    expect(document.documentElement.outerHTML).toContain(
      "It appears that pin is incorrect."
    );
  });

  it("Updates current balance after withdrawl", async () => {
    const app = mount(<App />);

    fetch.mockResponseOnce(JSON.stringify({ currentBalance: 300 }));

    await act(async () => {
      padButtonPresses(app, ["Enter"]);
    });
    app.update();

    padButtonPresses(app, ["5", "Enter"]);

    app.update();
    expect(
      app
        .find("p")
        .filterWhere((n) => n.text() === "£295.00")
        .text()
    ).toEqual("£295.00");
  });

  it("Displays alert that you have insufficent funds for withdrawl", async () => {
    const app = mount(<App />);

    fetch.mockResponseOnce(JSON.stringify({ currentBalance: 300 }));

    await act(async () => {
      padButtonPresses(app, ["Enter"]);
    });
    app.update();
    padButtonPresses(app, ["5", "5", "5", "Enter"]);

    app.update();
    expect(document.documentElement.outerHTML).toContain("Insufficent funds");
    expect(document.documentElement.outerHTML).toContain(
      "It appears you do <b>not have enough in your account</b> to withdaw these fund"
    );
  });
  +it("Displays alert that withdrawls must be in multiples of fives", async () => {
    const app = mount(<App />);

    fetch.mockResponseOnce(JSON.stringify({ currentBalance: 300 }));

    await act(async () => {
      padButtonPresses(app, ["Enter"]);
    });
    app.update();

    padButtonPresses(app, ["1", "Enter"]);

    app.update();
    expect(document.documentElement.outerHTML).toContain(
      "Please make sure the entered amount is in <b>multples of fives"
    );
  });
});

describe("Alert", () => {
  it("is hidden", () => {
    const app = mount(<Alert showAlert={false} />);

    expect(app.isEmptyRender()).toBeTruthy();
  });

  it("Shows an Alert", () => {
    const app = mount(<Alert showAlert={true} alertTitle="Test" />);

    expect(app.text()).toEqual("Test");
  });

  it("sets alert message and title", () => {
    const app = mount(
      <Alert showAlert={true} alertTitle="TestTitle " alertMsg="TestMsg" />
    );
    expect(app.text()).toEqual("TestTitle TestMsg");
  });
});

describe("Pad", () => {
  it("has 12 buttons on pad", () => {
    const pad = mount(<PinPad />);
    expect(pad.find("button").length).toEqual(12);
    pad.unmount();
  });

  it("has button press to enter pin", () => {
    const app = mount(<App />);

    padButtonPresses(app, ["1"]);
    expect(app.find("input").prop("value")).toEqual("1");
  });
});

describe("Pad Prompt", () => {
  it("Password is hidden", () => {
    const prompt = mount(<PadPrompt />);
    expect(prompt.find("input").prop("type")).toEqual("password");
    prompt.unmount();
  });

  it("reveals password", () => {
    const prompt = mount(<PadPrompt />);
    prompt.find("button").simulate("click");
    expect(prompt.find("input").prop("type")).toEqual("text");
    prompt.unmount();
  });

  it("shows password prompt on page load", () => {
    const prompt = mount(<PadPrompt />);
    expect(prompt.find("p").text()).toEqual("Please enter you pin below");
  });

  it("Shows withdrawl prompt when balance present", () => {
    const wrapper = shallow(<PadPrompt balance={100} withdrawlAmount={0} />);
    expect(
      wrapper
        .find("p")
        .filterWhere(
          (n) => n.text() === "Please enter how much you want to withdraw"
        )
        .text()
    ).toEqual("Please enter how much you want to withdraw");
  });
});
