import "./App.css";
import { useState } from "react";
import { signinRequest } from "./api";
import PinPad from "./components/Pad";
import Alert from "./components/Alert";
import LoadingIndicator from "./components/LoadingIndicator";
import PadPrompt from "./components/PadPrompt";

function App() {
  const [pin, setPin] = useState("");
  const [badLoginAlert, setBadLoginAlert] = useState(false);
  const [alertTitle, setAlertTitle] = useState();
  const [alertMsg, setAlertMsg] = useState();
  const [reqSent, setReqSent] = useState(false);
  const [balance, setBalance] = useState();
  const OVERDRAFTLIMIT = 100;
  const [notesAvaiable, setNotesAvaiable] = useState({
    "£5": 4,
    "£10": 15,
    "£20": 7,
  });
  const [withdrawlAmount, setWithdrawlAmount] = useState(0);
  const sendRequest = async () => {
    setReqSent(true);
    const response = await signinRequest(pin);
    if (!response) {
      setAlertTitle("Bad Login");
      setAlertMsg("It appears that pin is incorrect.");
      setBadLoginAlert(true);
    }
    setBalance(response.currentBalance);
    setReqSent(false);
  };

  const validateWithdrawlAmount = () => {
    if (withdrawlAmount % 5 !== 0) {
      setAlertTitle("Invalid amount entered");
      setAlertMsg(
        <>
          Please make sure the entered amount is in <b>multples of fives</b>
        </>
      );
      setBadLoginAlert(true);
      return false;
    } else if (balance - withdrawlAmount < -OVERDRAFTLIMIT) {
      setAlertTitle("Insufficent funds ");
      setAlertMsg(
        <>
          It appears you do <b>not have enough in your account</b> to withdaw
          these funds
        </>
      );
      setBadLoginAlert(true);
      return false;
    }
    return true;
  };

  const withdrawFunds = () => {
    const validAmount = validateWithdrawlAmount();
    var notesTotal = 0;
    var insufficentFunds;
    var amount = withdrawlAmount;
    const notes = { ...notesAvaiable };
    var handedNotes = {
      "£5": 0,
      "£10": 0,
      "£20": 0,
    };
    if (validAmount) {
      while (notesTotal !== Number(withdrawlAmount)) {
        if (amount / 20 >= 1 && notes["£20"] > 0) {
          handedNotes["£20"] = handedNotes["£20"] + 1;
          notesTotal = notesTotal + 20;
          amount = amount - 20;
          notes["£20"] = notes["£20"] - 1;
        }
        if (amount / 10 >= 1 && notes["£10"] > 0) {
          handedNotes["£10"] = handedNotes["£10"] + 1;
          notesTotal = notesTotal + 10;
          amount = amount - 10;
          notes["£10"] = notes["£10"] - 1;
        }
        if (amount / 5 >= 1 && notes["£5"] > 0) {
          handedNotes["£5"] = handedNotes["£5"] + 1;
          notesTotal = notesTotal + 5;
          amount = amount - 5;
          notes["£5"] = notes["£5"] - 1;
        }

        if (
          notes["£20"] === 0 &&
          notes["£10"] === 0 &&
          notes["£5"] === 0 &&
          amount !== 0
        ) {
          setAlertTitle("Unavailable Funds");
          setAlertMsg(
            <>
              It appears <b>this machines</b> does not have the avaiable funds
              to make this withdrawl
            </>
          );
          setBadLoginAlert(true);
          insufficentFunds = true;
          return;
        }
      }
      if (!insufficentFunds) {
        setNotesAvaiable(notes);
        setBalance(balance - withdrawlAmount);
        setBadLoginAlert(true);
        setAlertTitle("Successful withdawl");
        setAlertMsg(
          <>
            {handedNotes["£5"] > 0 ? (
              <>
                £5: {handedNotes["£5"]} <br />
              </>
            ) : null}
            {handedNotes["£10"] > 0 ? (
              <>
                £10: {handedNotes["£10"]} <br />
              </>
            ) : null}
            {handedNotes["£20"] > 0 ? <>£20: {handedNotes["£20"]}</> : null}
          </>
        );
      }
    }
  };

  const handleClose = () => {
    setBadLoginAlert(false);
  };

  const handlePinChange = (value) => {
    if (value !== "<" && value !== "Enter") {
      setPin(pin + value);
    } else if (value === "Enter") {
      sendRequest();
    } else {
      setPin(pin.slice(0, -1));
    }
  };

  const handleWithdawlAmountChange = (value) => {
    if (value !== "<" && value !== "Enter") {
      setWithdrawlAmount(withdrawlAmount + value);
    } else if (value === "Enter") {
      withdrawFunds();
    } else {
      setWithdrawlAmount(withdrawlAmount.slice(0, -1));
    }
  };

  return (
    <div className="App">
      <header className="App-header">
        <div>
          <Alert
            showAlert={badLoginAlert}
            handleClose={handleClose}
            alertMsg={alertMsg}
            alertTitle={alertTitle}
          />
          <LoadingIndicator show={reqSent} />
          <PadPrompt
            balance={balance}
            withdrawlAmount={withdrawlAmount}
            pin={pin}
          />
        </div>
        <PinPad
          handlePinChange={handlePinChange}
          handleWithdawlAmountChange={handleWithdawlAmountChange}
          balance={balance}
        />
      </header>
    </div>
  );
}

export default App;
