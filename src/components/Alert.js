import Modal from "@material-ui/core/Modal";
import Fade from "@material-ui/core/Fade";
import Backdrop from "@material-ui/core/Backdrop";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: "#fff",
  },
}));

const Alert = ({ showAlert, alertTitle, alertMsg, handleClose }) => {
  const classes = useStyles();
  return (
    <Modal
      className={classes.modal}
      open={showAlert}
      onClose={handleClose}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500,
      }}
    >
      <Fade in={showAlert}>
        <div className={classes.paper}>
          <h2 id="transition-modal-title">{alertTitle}</h2>
          <p id="transition-modal-description">{alertMsg}</p>
        </div>
      </Fade>
    </Modal>
  );
};

export default Alert;
