import { useState } from "react";
import { IconButton } from "@material-ui/core";
import Input from "@material-ui/core/Input";
import InputAdornment from "@material-ui/core/InputAdornment";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";

const PadPrompt = ({ balance, withdrawlAmount, pin }) => {
  const [passwordHidden, setPasswordHidden] = useState(true);
  return (
    <div>
      {balance ? (
        <div>
          <p>Current Balance</p>
          <p style={{ borderWidth: 10, borderColor: "white" }}>
            £{Number(balance).toFixed(2)}
          </p>
          <p>Please enter how much you want to withdraw</p>
          <p>£{Number(withdrawlAmount).toFixed(2)}</p>
        </div>
      ) : (
        <>
          <Input
            style={{ color: "white" }}
            type={passwordHidden ? "password" : "text"}
            readOnly
            value={pin}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  style={{ color: "white" }}
                  aria-label="toggle password visibility"
                  onClick={() => setPasswordHidden(!passwordHidden)}
                >
                  {passwordHidden ? <Visibility /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            }
          />
          <p>Please enter you pin below</p>
        </>
      )}
    </div>
  );
};

export default PadPrompt;
