import { Button, Grid, Card } from "@material-ui/core";

const PinPad = ({ balance, handlePinChange, handleWithdawlAmountChange }) => {
  const padValues = [
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "<",
    "0",
    "Enter",
  ];

  return (
    <Grid id="Pin Pad" container spacing={2} style={{ width: "50%" }}>
      {padValues.map((value) => {
        return (
          <Grid
            key={value}
            item
            xs={4}
            sm={4}
            md={4}
            lg={4}
            style={{ borderWidth: 5, borderColor: "black" }}
          >
            <Card raised>
              <Button
                fullWidth
                onClick={() => {
                  if (!balance) {
                    handlePinChange(value);
                  } else {
                    handleWithdawlAmountChange(value);
                  }
                }}
              >
                {value}
              </Button>
            </Card>
          </Grid>
        );
      })}
    </Grid>
  );
};

export default PinPad;
