export const signinRequest = async (pin) => {
  const url = "https://frontend-challenge.screencloud-michael.now.sh/api/pin/";
  const response = await fetch(url, {
    method: "POST",
    body: JSON.stringify({
      pin: pin,
    }),
    headers: {
      "Content-type": "application/json; charset=UTF-8",
    },
  });
  if (response.ok) {
    return await response.json();
  }
  return false;
};
